package com.deva.maddy.yahoochallenge.dao;

import java.util.ArrayList;

/**
 * Program DAO interface
 *
 * @author Maddy Deva
 */
public interface ProgramDao {
    /**
     * Returns the total number of programs that DAO has
     *
     * @return total number of programs
     */
    int getTotalNumberOfPrograms();
    /**
     * Returns all programs
     *
     * @return all program objects
     */
    ArrayList<Program> getPrograms();

    /**
     * Parses the passed in JSON string into Program POJO object
     *
     * @param jsonString string to be parsed
     */
    void parsePrograms(String jsonString);
}
