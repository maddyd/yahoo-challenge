package com.deva.maddy.yahoochallenge.common;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.deva.maddy.yahoochallenge.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Helper class to get the application configuration settings from property file
 *
 * @author Maddy Deva
 */
public class PropertyUtils {
    // To be used in logs
    private static final String LOG_TAG = PropertyUtils.class.getSimpleName();

    // Property names
    private static final String WHATSBEEF_SERVER_ADDRESS = "whatsbeef_serveraddress";

    // Instance variables
    private static PropertyUtils sPropertyUtils;
    private static Properties sProperties;
    private Context context;

    /**
     * Force it to be singleton class
     *
     * @param context context in which this class is run
     */
    private PropertyUtils(Context context) {
        this.context = context;
    }

    /**
     * Returns the singleton instance
     */
    public static PropertyUtils getInstance(Context ctx) {
        if(sPropertyUtils == null) {
            // Use application context
            sPropertyUtils = new PropertyUtils(ctx.getApplicationContext());
        }

        return sPropertyUtils;
    }

    /**
     * Returns the whatsbeef server address property
     *
     * @return whatsbeef server address
     * @throws ApplicationConfigurationException
     */
    public String getWhatsbeefServerAddress() throws ApplicationConfigurationException {
        loadProperties();
        // Return the server address
        return sProperties.getProperty(WHATSBEEF_SERVER_ADDRESS);
    }

    /**
     * Loads the properties from config file
     * @throws ApplicationConfigurationException
     */
    private void loadProperties() throws ApplicationConfigurationException {
        // Set the properties if not done already
        if (sProperties == null) {
            Resources resources = context.getResources();
            InputStream appConfigProperties = resources.openRawResource(R.raw.app_config);
            sProperties = new Properties();

            // Read properties file.
            try {
                sProperties.load(appConfigProperties);
            } catch (IOException e) {
                Log.e(LOG_TAG, "failed to find the properties file");
                throw new ApplicationConfigurationException();
            }
        }
    }

    public static class ApplicationConfigurationException extends Exception {
        private static final long serialVersionUID = -5361718032142552653L;
    }
}
