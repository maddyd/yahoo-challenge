package com.deva.maddy.yahoochallenge;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.deva.maddy.yahoochallenge.dao.Program;
import com.deva.maddy.yahoochallenge.dao.ProgramDaoImpl;

import java.util.ArrayList;

/**
 * Adapater for list of programs
 *
 * @author Maddy Deva
 */
public class ProgramsListViewAdapter extends BaseAdapter {

    /**
     * Callback interface from this adapter.
     * This callback is invoked when this adapter reaches the end of the list view
     */
    public interface ProgramsListViewAdapterListener {
        void onLoadMorePrograms(int startIndex);
    }

    // Instance variables
    private Context context;
    private ArrayList<Program> programs;
    private ProgramsListViewAdapterListener listener;

    public ProgramsListViewAdapter(Context context, ProgramsListViewAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        programs = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return programs.size();
    }

    @Override
    public Object getItem(int position) {
        return programs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        // Listview optimisation, if the item view is already present just
        // get the existing object instead of doing the costly inflating operation
        if(convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.listview_cell, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.channelName = (TextView) convertView.findViewById(R.id.channelName);
            viewHolder.programName = (TextView) convertView.findViewById(R.id.programName);
            viewHolder.startTime = (TextView) convertView.findViewById(R.id.startTime);
            viewHolder.endTime = (TextView) convertView.findViewById(R.id.endTime);
            viewHolder.rating = (TextView) convertView.findViewById(R.id.rating);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Get the program object and populate UI elements with it's data
        Program program = programs.get(position);
        viewHolder.channelName.setText(program.getChannel());
        viewHolder.programName.setText(program.getName());
        viewHolder.startTime.setText(program.getStartTime());
        viewHolder.endTime.setText(program.getEndTime());
        viewHolder.rating.setText(program.getRating());

        // If it is the last item in the list, invoke the callback to
        // get more items
        if(position == programs.size() - 1)
        {
            listener.onLoadMorePrograms(programs.size());
        }

        return convertView;
    }

    static class ViewHolder {
        TextView channelName;
        TextView programName;
        TextView startTime;
        TextView endTime;
        TextView rating;
    }

    /**
     * Refreshes the programs list view with items from programs DAO
     */
    public void refresh() {
        programs = ProgramDaoImpl.getInstance().getPrograms();
        notifyDataSetChanged();
    }
}
