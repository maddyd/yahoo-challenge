package com.deva.maddy.yahoochallenge.network;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Helper class to make network calls and get the response from the server
 * NOTE: Currently only GET response method is implemented as the challenge only asks to execute this
 * response
 *
 * @author Maddy Deva
 */
public class HttpHelper {
    // TAG to be used in logs
    private static final String LOG_TAG = HttpHelper.class.getSimpleName();

    // Read timeout in milliseconds
    private static final int READ_TIMEOUT = 10000;
    // Connection timeout in milliseconds
    private static final int CONNECTION_TIMEOUT = 15000;

    /**
     * Returns response string from the resource pointed out by urlString
     *
     * @param urlString resource address i.e., link
     * @return response string
     */
    public static String getResponse(String urlString) {
        HttpURLConnection urlConnection = null;
        InputStream is;
        String responseStr = null;

        try {
            // Create connection
            URL url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            // This is a GET call
            urlConnection.setRequestMethod("GET");
            // We are interested in JSON formatted response
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setUseCaches(false);
            urlConnection.setReadTimeout(READ_TIMEOUT);
            urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);

            // Make the request
            urlConnection.connect();
            int responseCode = urlConnection.getResponseCode();
            Log.d(LOG_TAG, "The response is: " + responseCode);
            // Get Response
            is = urlConnection.getInputStream();
            BufferedReader brd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuilder response = new StringBuilder();
            while ((line = brd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            brd.close();
            responseStr = response.toString();
            Log.d(LOG_TAG,responseStr);
        } catch (Exception e) {
            Log.e(LOG_TAG, "Exception occurred in getting the response");
            e.printStackTrace();
        } finally {
            // Finally do the cleanup i.e., close the input stream
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return responseStr;
    }
}