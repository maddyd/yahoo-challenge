package com.deva.maddy.yahoochallenge.dao;

/**
 * Program POJO
 *
 * @author Maddy Deva
 */
public class Program {
    // Program name
    private String name;
    // Program start time
    private String startTime;
    // Program end time
    private String endTime;
    // Channel that's the program is on
    private String channel;
    // Program rating
    private String rating;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
