package com.deva.maddy.yahoochallenge.dao;

/**
 * Represents the Program JSON object fields
 *
 * @author Maddy Deva
 */
public enum ProgramJsonFields {
    results,
    count,
    name,
    start_time,
    end_time,
    channel,
    rating
}
