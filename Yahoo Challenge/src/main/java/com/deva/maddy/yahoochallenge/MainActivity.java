package com.deva.maddy.yahoochallenge;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.deva.maddy.yahoochallenge.ProgramsListViewAdapter.ProgramsListViewAdapterListener;
import com.deva.maddy.yahoochallenge.asynctask.GetMoreDataFromServerAsycTask;
import com.deva.maddy.yahoochallenge.asynctask.GetMoreDataFromServerAsycTaskCallback;
import com.deva.maddy.yahoochallenge.dao.ProgramDaoImpl;

/**
 * MainActivity that shows the list of TV programs
 *
 * @author Maddy Deva
 */
public class MainActivity extends AppCompatActivity implements ProgramsListViewAdapterListener, GetMoreDataFromServerAsycTaskCallback {

    // View objects
    private ListView programsListView;
    private ProgressBar loadMoreProgramProgressBar;

    // Holds information whether we are currently getting more items
    private boolean isGettingMoreItems = false;
    // AsyncTask that gets more items from server
    private GetMoreDataFromServerAsycTask getMoreDataFromServerAsycTask;

    // List view adapter
    private ProgramsListViewAdapter programsListViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize layout
        initLayout();

        // Initially load programs with start index 0
        onLoadMorePrograms(0);
    }

    @Override
    protected void onPause() {
        // Cancel the AsyncTask if it is running
        if (isGettingMoreItems) {
            // Cancel the AsyncTask
            if (getMoreDataFromServerAsycTask != null) {
                getMoreDataFromServerAsycTask.cancel(true);
            }

            // Reset the UI
            resetUI();
        }
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        // Consume the back press if we are in getting more state and cancel the asynctask.
        // Otherwise pass it on to parent to handle it normally
        if (isGettingMoreItems) {
            // Cancel the AsyncTask
            if (getMoreDataFromServerAsycTask != null) {
                getMoreDataFromServerAsycTask.cancel(true);
            }

            // Reset the UI
            resetUI();
        } else {
            super.onBackPressed();
        }
    }

    // Initializes UI elements
    private void initLayout() {
        // Get progress bar instance
        loadMoreProgramProgressBar = (ProgressBar) findViewById(R.id.loadProgress);

        // Initialize listview object
        programsListView = (ListView) findViewById(R.id.listview);
        programsListViewAdapter = new ProgramsListViewAdapter(this, this);
        programsListView.setAdapter(programsListViewAdapter);
    }


    // Executes AsyncTask with passed in startIndex
    private void executeAsyncTast(int startIndex) {
        // Create new task
        getMoreDataFromServerAsycTask = new GetMoreDataFromServerAsycTask(this, this);
        // Execute the task
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
            getMoreDataFromServerAsycTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, startIndex);
        } else {
            getMoreDataFromServerAsycTask.execute(startIndex);
        }
    }

    // Resets the UI
    private void resetUI() {
        // We are finished with getting more items
        isGettingMoreItems = false;
        // Dismiss busy progress bar
        loadMoreProgramProgressBar.setVisibility(View.GONE);
        // Enable back the ListView control so that user can interact with it
        programsListView.setEnabled(true);
    }

    // ==========================================================
    // ProgramsListViewAdapterListner implementation start
    // ==========================================================
    @Override
    public void onLoadMorePrograms(int startIndex) {
        // Load more items only if start index is zero or less than total number of programs available
        int totalNumberOfPrograms = ProgramDaoImpl.getInstance().getTotalNumberOfPrograms();
        if (startIndex == 0 || startIndex < totalNumberOfPrograms) {
            // Display busy progress bar
            loadMoreProgramProgressBar.setVisibility(View.VISIBLE);
            // Disable ListView from any user interactions e.g., scrolling while loading more programs
            programsListView.setEnabled(false);

            // Now execute AsyncTask to get more items from server
            executeAsyncTast(startIndex);

            // We are now getting more items
            isGettingMoreItems = true;
        }
    }
    // ==========================================================
    // ProgramsListViewAdapterListner implementation end
    // ==========================================================

    // ==========================================================
    // GetMoreDataFromServerAsycTaskCallback implementation start
    // ==========================================================
    public void onTaskFinsh() {
        // Refresh the list items first
        programsListViewAdapter.refresh();
        // Reset list UI
        resetUI();
    }
    // ==========================================================
    // GetMoreDataFromServerAsycTaskCallback implementation end
    // ==========================================================
}
