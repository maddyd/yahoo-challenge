package com.deva.maddy.yahoochallenge.asynctask;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.deva.maddy.yahoochallenge.common.PropertyUtils;
import com.deva.maddy.yahoochallenge.common.PropertyUtils.ApplicationConfigurationException;
import com.deva.maddy.yahoochallenge.dao.ProgramDao;
import com.deva.maddy.yahoochallenge.dao.ProgramDaoImpl;
import com.deva.maddy.yahoochallenge.network.HttpHelper;

/**
 * AsyncTask that gets more data from whatsbeef server
 *
 * @author Maddy Deva
 */
public class GetMoreDataFromServerAsycTask extends AsyncTask<Integer, Void, Void> {
    // TAG to be used in logs
    private static final String LOG_TAG = GetMoreDataFromServerAsycTask.class.getSimpleName();

    private static final String SERVER_DATA_PATH = "wabz/guide.php?start=";

    // Instance variables
    private AppCompatActivity activity;
    private GetMoreDataFromServerAsycTaskCallback callback;
    private PropertyUtils propertyUtils;

    public GetMoreDataFromServerAsycTask(AppCompatActivity activity, GetMoreDataFromServerAsycTaskCallback callback) {
        super();
        this.activity = activity;
        this.callback = callback;
        propertyUtils = PropertyUtils.getInstance(activity);
    }

    @Override
    protected Void doInBackground(Integer... startQueryParam) {
        try {
            // Form the full whatsbeef url to get more data from server
            String forecastIOUrl = propertyUtils.getWhatsbeefServerAddress() + SERVER_DATA_PATH + startQueryParam[0];
            // Execute the GET request and get the response from server
            String jsonString = HttpHelper.getResponse(forecastIOUrl);
            Log.d(LOG_TAG, "jsonString = " + jsonString);
            // Parse the return json string
            ProgramDao programDao = ProgramDaoImpl.getInstance();
            programDao.parsePrograms(jsonString);
        } catch (ApplicationConfigurationException ace) {
            Log.e(LOG_TAG, "ApplicationConfigurationException has occurred");
            ace.printStackTrace();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Exception has occurred");
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        // Invoke the callback only if the activity that created this task is still running
        // and callback is not null
        if(!activity.isFinishing() && callback != null) {
            // We are done, invoke the callback
            callback.onTaskFinsh();
        }
    }
}
