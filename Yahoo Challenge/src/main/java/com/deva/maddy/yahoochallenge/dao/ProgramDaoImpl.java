package com.deva.maddy.yahoochallenge.dao;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Singleton Program DAO implementation
 *
 * @author Maddy Deva
 */
public class ProgramDaoImpl implements ProgramDao {
    // Log tag
    private static final String LOG_TAG = ProgramDaoImpl.class.getSimpleName();

    // Total number of programs
    private int totalNumberOfPrograms = 0;
    // Programs
    private ArrayList<Program> programs;

    // Only one instance of this class
    private static ProgramDao sInstance;

    // Sigleton
    private ProgramDaoImpl() {
        programs = new ArrayList<>();
    }

    // Returns only one object
    public static ProgramDao getInstance() {
        if (sInstance == null) {
            sInstance = new ProgramDaoImpl();
        }

        return sInstance;
    }

    @Override
    public int getTotalNumberOfPrograms() {
        return totalNumberOfPrograms;
    }

    @Override
    public ArrayList<Program> getPrograms() {
        return programs;
    }

    @Override
    public void parsePrograms(String jsonString) {
        try {
            // Form the parent JSON object
            JSONObject parentObject = new JSONObject(jsonString);
            totalNumberOfPrograms = parentObject.getInt(ProgramJsonFields.count.name());
            JSONArray resultsJsonArray = parentObject.getJSONArray(ProgramJsonFields.results.name());
            for(int i = 0; i < resultsJsonArray.length(); i++) {
                // Get result JSON object
                JSONObject resultJsonObject = resultsJsonArray.getJSONObject(i);

                // Build Program POJO object
                Program program = new Program();
                program.setChannel(resultJsonObject.getString(ProgramJsonFields.channel.name()));
                program.setName(resultJsonObject.getString(ProgramJsonFields.name.name()));
                program.setStartTime(resultJsonObject.getString(ProgramJsonFields.start_time.name()));
                program.setEndTime(resultJsonObject.getString(ProgramJsonFields.end_time.name()));
                program.setRating(resultJsonObject.getString(ProgramJsonFields.rating.name()));
                // Add the program object to list of programs
                programs.add(program);
            }

        } catch (JSONException e) {
            Log.e(LOG_TAG, "JSONException occurred");
            e.printStackTrace();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Exception occurred");
            e.printStackTrace();
        }
    }
}
