package com.deva.maddy.yahoochallenge.asynctask;

/**
 * GetMoreDataFromServerAsycTaskCallback interface
 *
 * @author Maddy Deva
 */
public interface GetMoreDataFromServerAsycTaskCallback {
    void onTaskFinsh();
}
